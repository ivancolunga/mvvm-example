package mx.com.ivan.mvvmproject.mainscreen

import io.reactivex.rxjava3.core.Observable
import retrofit2.Response
import retrofit2.http.GET

interface PostService {

    @GET("posts")
    fun getPosts(): Observable<Response<List<PostItemResponse>>>
}