package mx.com.ivan.mvvmproject.mainscreen

import dagger.Component
import mx.com.ivan.mvvmproject.core.AppScope
import mx.com.ivan.mvvmproject.core.ApplicationComponent
import javax.inject.Scope

@AppScope
@Component(
    dependencies = [ApplicationComponent::class],
    modules = [HomeModule::class]
)
interface HomeComponent {
    fun inject(fragment: HomeFragment)
}