package mx.com.ivan.mvvmproject.mainscreen

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import io.reactivex.rxjava3.android.schedulers.AndroidSchedulers
import io.reactivex.rxjava3.schedulers.Schedulers

class HomeViewModel(
    application: Application,
    private val repository: HomeRepository
) : AndroidViewModel(application) {

    val getPostState: MutableLiveData<List<PostItemResponse>> by lazy {
        MutableLiveData<List<PostItemResponse>>()
    }

    fun getPosts() {
        repository.getPosts()
            .observeOn(AndroidSchedulers.mainThread())
            .subscribeOn(Schedulers.io())
            .subscribe {
                getPostState.value = it
            }
    }

}