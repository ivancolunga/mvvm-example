package mx.com.ivan.mvvmproject

import android.app.Application
import android.content.Context
import mx.com.ivan.mvvmproject.core.AppModule
import mx.com.ivan.mvvmproject.core.ApplicationComponent
import mx.com.ivan.mvvmproject.core.DaggerApplicationComponent
import mx.com.ivan.mvvmproject.core.NetModule

class MyApplication : Application() {

    val applicationComponent: ApplicationComponent by lazy {
        DaggerApplicationComponent.builder()
            .appModule(AppModule(this))
            .netModule(NetModule())
            .build()
    }


}

val Context.applicationComponent: ApplicationComponent get()  {
    if (this.applicationContext !is MyApplication) {
        throw IllegalArgumentException("Context provided is out of app context.")
    }
    val app = this.applicationContext as MyApplication
    return app.applicationComponent
}