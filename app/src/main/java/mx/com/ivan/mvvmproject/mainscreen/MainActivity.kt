package mx.com.ivan.mvvmproject.mainscreen

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import mx.com.ivan.mvvmproject.R
import mx.com.ivan.mvvmproject.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        showFragment()
    }

    private fun showFragment() {
        supportFragmentManager
            .beginTransaction()
            .add(R.id.main_container, HomeFragment())
            .commit()

    }

}