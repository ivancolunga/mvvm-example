package mx.com.ivan.mvvmproject.mainscreen

import com.google.gson.annotations.SerializedName

data class PostItemResponse(
    @SerializedName("userId")
    private val _userId: String? = null,
    @SerializedName("id")
    private val _id: Int? = null,
    @SerializedName("title")
    private val _title: String? = null,
    @SerializedName("body")
    private val _body: String? = null,
) {
    val userId: String get() = _userId ?: ""
    val id: Int get() = _id ?: 0
    val title: String get() = _title ?: ""
    val body: String get() = _body ?: ""
}