package mx.com.ivan.mvvmproject.mainscreen

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit
import javax.inject.Scope

@Module
class HomeModule {

    @Provides
    fun provideService(retrofit: Retrofit): PostService =
        retrofit.create(PostService::class.java)

}