package mx.com.ivan.mvvmproject.mainscreen

import io.reactivex.rxjava3.core.Observable
import java.lang.Exception
import javax.inject.Inject

class HomeRepository @Inject constructor(
    private val service: PostService
) {

    fun getPosts(): Observable<List<PostItemResponse>> = service.getPosts().flatMap {
        if (it.isSuccessful) {
            Observable.just(it.body())
        } else {
            Observable.error(Exception(it.errorBody().toString()))
        }
    }

}