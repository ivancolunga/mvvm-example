package mx.com.ivan.mvvmproject.mainscreen

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import mx.com.ivan.mvvmproject.R

class PostAdapter(
    private val data: List<PostItemResponse>
) : RecyclerView.Adapter<PostAdapter.ItemViewHolder>() {

    inner class ItemViewHolder(view: View) : RecyclerView.ViewHolder(view) {
        val tvName: TextView = view.findViewById(R.id.post_name)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder =
        ItemViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.item_post, parent, false))

    override fun onBindViewHolder(holder: ItemViewHolder, position: Int) = with(holder) {
        val post = data[position]
        tvName.text = post.title
    }
    override fun getItemCount(): Int = data.size
}