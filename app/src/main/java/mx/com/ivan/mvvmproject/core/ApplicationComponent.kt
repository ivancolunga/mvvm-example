package mx.com.ivan.mvvmproject.core

import android.app.Application
import android.content.res.Resources
import dagger.Component
import retrofit2.Retrofit
import javax.inject.Singleton

@Singleton
@Component(
    modules = [AppModule::class, NetModule::class]
)
interface ApplicationComponent {

    fun applicationContext(): Application

    fun resources(): Resources

    fun retrofit(): Retrofit

}