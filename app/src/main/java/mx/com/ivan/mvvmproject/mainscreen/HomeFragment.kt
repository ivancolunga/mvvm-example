package mx.com.ivan.mvvmproject.mainscreen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import mx.com.ivan.mvvmproject.applicationComponent
import mx.com.ivan.mvvmproject.databinding.FragmentHomeBinding
import javax.inject.Inject

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null

    private val binding: FragmentHomeBinding get() = _binding!!

    @Inject
    lateinit var vmFactory: HomeViewModelFactory

    private val component: HomeComponent by lazy {
        DaggerHomeComponent.builder()
            .applicationComponent(requireActivity().applicationComponent)
            .homeModule(HomeModule())
            .build()
    }

    private val viewModel by lazy {
        ViewModelProvider(this, vmFactory).get(HomeViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        _binding = FragmentHomeBinding.inflate(layoutInflater)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        component.inject(this)
        viewModel.getPostState.observe(viewLifecycleOwner) {

            binding.rvPosts.adapter = PostAdapter(it)

        }
        viewModel.getPosts()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}